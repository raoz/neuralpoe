#!/usr/bin/env python3
#Based on code by Andrej Karpathy
import numpy as np
import itertools

data = open("data.txt", 'r').read() #plaintext
vocab = list(set(data))
vocab_size=len(vocab)
elem_index_map = {e:i for i, e in enumerate(vocab)}
index_elem_map = {i:e for i, e in enumerate(vocab)}

sample_len = 250

hidden_size = 512

seq_len = 512
learning_rate = 0.1




Wxh = np.random.randn(hidden_size, vocab_size) * 0.01 #input to hidden
Whh = np.random.randn(hidden_size, hidden_size) * 0.01 #hidden to hidden
Why = np.random.randn(vocab_size, hidden_size) * 0.01 #hidden to output

bh = np.zeros((hidden_size, 1)) #hidden bias
by = np.zeros((vocab_size, 1)) #output bias

def lossFun(inputs, targets, hprev):
    """
        intlists input, targets
        H*1 array hprev: previous hidden size
        returns loss, gradients, new hidden state
    """
    xs, hs, ys, ps = {}, {}, {}, {}
    hs[-1] = np.copy(hprev)
    loss = 0

    #forward pass
    for t in range(len(inputs)):
        xs[t] = np.zeros((vocab_size, 1)) #xs is one-of-k
        xs[t][inputs[t]] = 1
        hs[t] = np.tanh(np.dot(Wxh, xs[t]) + np.dot(Whh, hs[t-1]) + bh) #compute hidden state
        ys[t] = np.dot(Why, hs[t]) + by #log probabilities for next chars(unnormalised)
        nonorm_ps = np.exp(ys[t])
        ps[t] = nonorm_ps / np.sum(nonorm_ps) #actual probabilities
        loss += -np.log(ps[t][targets[t], 0]) #cross-entropy loss

    #backward pass
    dWxh, dWhh, dWhy = np.zeros_like(Wxh), np.zeros_like(Whh), np.zeros_like(Why)
    dbh, dby = np.zeros_like(bh), np.zeros_like(by)

    dhnext = np.zeros_like(hs[0])
    for t in reversed(range(len(inputs))):
        dy = np.copy(ps[t])
        dy[targets[t]] -= 1 #backpropagation into y
        dWhy += np.dot(dy, hs[t].T)
        dby += dy
        dh = np.dot(Why.T, dy) + dhnext #backpropagation into h
        dhraw = (1 - hs[t] * hs[t]) * dh #account for tanh
        dbh += dhraw
        dWxh += np.dot(dhraw, xs[t].T)
        dWhh += np.dot(dhraw, hs[t-t].T)
        dhnext = np.dot(Whh.T, dhraw)
    for dparam in [dWxh, dWhh, dWhy, dbh, dby]:
        np.clip(dparam, -5, 5, out=dparam) #avoid gradient explosion
    return loss, dWxh, dWhh, dWhy, dbh, dby, hs[len(inputs) - 1]

def sample(h, seed_i, n):
    """
    samples a sequence of n letters
    h: memory state 
    seed_i: seed letter for first step
    returns indexes of letters
    """
    x = np.zeros((vocab_size, 1)) # input layer
    x[seed_i] = 1 
    list_i = []
    for t in range(n):
        h = np.tanh(np.dot(Wxh, x) + np.dot(Whh, h) + bh)
        y = np.dot(Why, h) + by
        nonorm_p = np.exp(y)
        p = nonorm_p / np.sum(nonorm_p)
        i = np.random.choice(range(vocab_size), p=p.ravel())
        x = np.zeros((vocab_size, 1))
        x[i] = 1
        list_i.append(i)
    return list_i



mWxh, mWhh, mWhy = np.zeros_like(Wxh), np.zeros_like(Whh), np.zeros_like(Why)
mbh, mby = np.zeros_like(bh), np.zeros_like(by) #mem vals for Adagrad
smooth_loss = -np.log(1.0/vocab_size*seq_len) #starting loss
data_index = 0
for it in itertools.count():
    #prep inputs
    if data_index + seq_len + 1 >= len(data) or it == 0:
        hprev = np.zeros((hidden_size, 1)) #reset memory
        data_index = 0
    inputs = [elem_index_map[ch] for ch in data[data_index:data_index+seq_len]]
    targets = [elem_index_map[ch] for ch in data[data_index+1:data_index+seq_len+1]]

    #samples
    if it % 100 == 0:
        i_sample = sample(hprev, inputs[0], sample_len)
        txt = ''.join(index_elem_map[i] for i in i_sample)
        print('-----\n%s' % (txt,))

    #go forward seq_len chars and get gradients and new hidden state
    loss, dWxh, dWhh, dWhy, dbh, dby, hprev = lossFun(inputs, targets, hprev)
    smooth_loss = smooth_loss*0.999+loss*0.001
    print("Iteration %d, loss: %f" % (it, smooth_loss))
    #Adagrad
    for param, dparam, mem in zip([Wxh, Whh, Why, bh, by],
                                  [dWxh, dWhh, dWhy, dbh, dby],
                                  [mWxh, mWhh, mWhy, mbh, mby]):
        mem += dparam * dparam
        param += -learning_rate * dparam / np.sqrt(mem + 1e-8)

    data_index += seq_len



